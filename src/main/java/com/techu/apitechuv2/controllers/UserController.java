package com.techu.apitechuv2.controllers;

import com.techu.apitechuv2.models.UserModel;
import com.techu.apitechuv2.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class UserController {

    @Autowired
    UserService UserService;

    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers( @RequestParam (name="$orderby", required = false) String orderBy){
        System.out.println("getUsers");
        System.out.println("El parametro de ordenacion es: "+orderBy);
        return new ResponseEntity<>(this.UserService.getUsers(orderBy), HttpStatus.OK);
    }

    @GetMapping("/users/{id}")
    public  ResponseEntity<Object> getUserById(@PathVariable String id){
        System.out.println("getUserById");
        System.out.println("id es "+id);
        Optional<UserModel> result = this.UserService.findUserById(id);
        return new ResponseEntity<>(result.isPresent() ? result.get() : "Usuario no encontrado", result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @PostMapping("/users")
    public ResponseEntity<UserModel>addUser(@RequestBody UserModel newUser){
        System.out.println("createUser");
        System.out.println("La ID del usuario que se va a crear es "+newUser.getId());
        System.out.println("La nombre del usuario que se va a crear es "+newUser.getName());
        System.out.println("El edad del usuario que se va a crear es "+newUser.getAge());
        return new ResponseEntity<>(this.UserService.addUser(newUser), HttpStatus.CREATED);
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<UserModel> updateUser(@PathVariable String id, @RequestBody UserModel user){
        System.out.println("updateUser");
        System.out.println("La id recibida por parametro es: " + id);
        System.out.println("La id del usuario a atualizar es: " + user.getId());
        System.out.println("El nombre del usuario a atualizar es: " + user.getName());
        System.out.println("La edad del usuario a atualizar es: " + user.getAge());
        UserModel result = this.UserService.updateUser(user);
        return new ResponseEntity<UserModel>(result, result!=null ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteUser (@PathVariable String id) {
        System.out.println("deleteUser");
        System.out.println("La id recibida por parametro es: " + id);
        boolean deletedUser = this.UserService.deleteUser(id);
        return new ResponseEntity<>(deletedUser ? "El usuario se ha borrado" : "Usuario no encontrado", deletedUser ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @PatchMapping("/users/{id}")
    public ResponseEntity<Object> patchUser (@PathVariable String id, @RequestBody UserModel user){
        System.out.println("patchUser");
        System.out.println("La id recibida por parametro es: " + id);
        System.out.println("El nombre del usuario a actualizar es: " + user.getName());
        System.out.println("La edad del usuario a actualizar es: " + user.getAge());
        UserModel result = this.UserService.partialUpdateUser(user);
        return new ResponseEntity<>(result!=null ? "Usuario Actualizado correctamente" : "Usuario no encontrado", result!=null ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }
}
