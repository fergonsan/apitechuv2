package com.techu.apitechuv2.services;

import com.techu.apitechuv2.models.UserModel;
import com.techu.apitechuv2.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public List<UserModel> getUsers(String orderBy){
        System.out.println("getUsers");
        if (orderBy!=null){
            System.out.println("Se ha solicitado ordenado");
            return this.userRepository.findAll(Sort.by(Sort.Direction.ASC, "age"));
        }else{
            return this.userRepository.findAll();
        }
    }
    public UserModel addUser(UserModel product){
        System.out.println("addUser");
        return this.userRepository.save(product);
    }
    public Optional<UserModel> findUserById(String id){
        System.out.println("findUserById");
        System.out.println("Obteniendo el usuario con la id "+id);
        return this.userRepository.findById(id);
    }
    public boolean deleteUser(String id){
        System.out.println("deleteUser");
        if (this.userRepository.findById(id).isPresent()){
            this.userRepository.deleteById(id);
            return true;
        }
        return false;
    }
    public UserModel updateUser(UserModel updateUser){
        System.out.println("updateUser");
        Optional<UserModel> result = findUserById(updateUser.getId());
        if (result.isPresent()){
            System.out.println("Usuario encontrado, actualizando...");
            return this.userRepository.save(updateUser);
        }
        System.out.println("Usuario no encontrado.");
        return null;

    }
    public UserModel partialUpdateUser(UserModel user){
        System.out.println("partialUpdateUser");
        UserModel result = new UserModel();
        Optional<UserModel> userInDb = findUserById(user.getId());
        if (userInDb.isPresent()){
            result = userInDb.get();
            if (user.getName()!=null) {
                System.out.println("Actualizando nombre");
                result.setName(user.getName());
            }
            if (user.getAge() > 0) {
                System.out.println("Actualizando edad");
                result.setAge(user.getAge());
            }
            return this.userRepository.save(result);
        }
        return null;
    }
}
