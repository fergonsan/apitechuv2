package com.techu.apitechuv2.services;

import com.techu.apitechuv2.models.ProductModel;
import com.techu.apitechuv2.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    public List<ProductModel> findAll(){
        return this.productRepository.findAll();
    }

    public ProductModel add (ProductModel product){
        System.out.println("add");
        return this.productRepository.save(product);
    }
    public Optional<ProductModel> findById (String id){
        System.out.println("findById");
        System.out.println("Obteniendo el producto con la id "+id);
        return this.productRepository.findById(id);
    }
    public boolean delete (String id){
        System.out.println("removeById");
        if (this.productRepository.findById(id).isPresent()){
            this.productRepository.deleteById(id);
            return true;
        }
        return false;
    }
    public ProductModel update (ProductModel updateProduct){
        System.out.println("updateById");
        Optional<ProductModel> result = findById(updateProduct.getId());
        if (result.isPresent()){
            return this.productRepository.save(updateProduct);
        }else {
            return null;
        }
    }
    public ProductModel partialUpdate (ProductModel product){
        System.out.println("partialUpdate");
        ProductModel result = new ProductModel();
        Optional<ProductModel> productInDb = findById(product.getId());
        if (productInDb.isPresent()){
            result = productInDb.get();
            if (product.getDesc()!=null) {
                System.out.println("Actualizando descripción");
                result.setDesc(product.getDesc());
            }
            if (product.getPrice() > 0) {
                System.out.println("Actualizando precio");
                result.setPrice(product.getPrice());
            }
            return this.productRepository.save(result);
        }
        return null;
    }
}
